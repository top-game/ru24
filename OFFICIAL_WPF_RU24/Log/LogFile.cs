﻿using System;
using System.IO;
using System.Linq;

namespace OFFICIAL_WPF_RU24.Log
{
    /// <summary>
    /// Файл лога
    /// </summary>
    public class LogFile
    {
        private string logfilename;
        //private StreamWriter sw;
        /// <summary>
        /// Представляет лог файл
        /// </summary>
        /// <param name="head">Название в начале файла</param>
        public LogFile(string head)
        {
            try
            {
                if (!Directory.Exists(string.Format("{0}\\logs\\", Environment.CurrentDirectory)))
                {
                    Directory.CreateDirectory(string.Format("{0}\\logs\\", Environment.CurrentDirectory));
                }
                this.logfilename = string.Format("{0}\\logs\\{1}_{2}.log", Environment.CurrentDirectory, head, (DateTime.Today.Millisecond + DateTime.Now.Second).GetHashCode());
                //sw = File.CreateText(this.logfilename);
                Console.WriteLine("Лог файл успешно создан. Его путь: " + logfilename);
            }
            catch (Exception exc)
            {
                Console.WriteLine(string.Format("Возникла проблема при создании лог-файла! {0}", exc.Message));   
            }
        }

        public void Write(string arg, string desc)
        {
            try
            {
                if (this.logfilename != null)
                {
                    Console.WriteLine(String.Format("[{0}]: {1}", arg, desc));
                    StreamWriter sw = new StreamWriter(logfilename);
                    sw.WriteLineAsync(String.Format("[{0}]: {1}", arg, desc));
                    sw.Close();
                    sw.Dispose();
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(string.Format("Возникла проблема при записи в лог-файл!  {0}", exc.Message));
            }
        }
    }
}