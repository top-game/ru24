﻿using OFFICIAL_WPF_RU24.Properties;
using System;
using System.Linq;
using System.Windows;



namespace OFFICIAL_WPF_RU24
{
   
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Лог файл приложения
        /// </summary>
        public static Log.LogFile lf = new Log.LogFile("ru24");

        public static MainWindow mw = null;

        //Types.Windows.LoadingForm LoadForm = new Types.Windows.LoadingForm();
        public App()
        {
            //Форма загрузки

            
            //LoadForm.Show();
            
            //lf = new Log.LogFile("ru24");
            lf.Write("Program", "Нахождение в процессе загрузки, добавление ивентов при крашах!");

            App.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Settings.Default.SettingsSaving += delegate
            {

            };
            
            //Application.Current.Run(new MainWindow());

            //LoadForm.Wait(10);

            //LoadForm.Hide();

            
            //SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);



            //throw new Exception("HAHAHA");
        }

     
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Utilites.Dump.GenerateDump((Exception)e.ExceptionObject);
            Utilites.Dump.CreateMiniDump();
        }

        void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Utilites.Dump.GenerateDump(e.Exception);
            Utilites.Dump.CreateMiniDump();
        }




    }
}
