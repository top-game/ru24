﻿using System;
using System.Linq;

namespace OFFICIAL_WPF_RU24.Types
{
    /// <summary>
    /// Текущий пользователь
    /// </summary>
    public class CurrentUser
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Ссылка на аватарку пользователя
        /// </summary>
        public Uri UserAvatar { get; set; }


        /// <summary>
        /// Change current user
        /// </summary>
        public CurrentUser(string FirstName, string LastName, Uri UserAvatar){
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.UserAvatar = UserAvatar;
        }

        public int UID { get; set; }

    }
}
