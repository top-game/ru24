﻿using System;
using System.Linq;

namespace OFFICIAL_WPF_RU24.Types
{
    public class Servers
    {
        public dynamic response;
    }
    public class Server
    {
        dynamic server = null;
        public Server(Types.Minecraft.Server s){
            server = s;
        }
        public Server(Types.DayZ.Server s){
            server = s;
        }

        public string type(FormatType ft)
        {
            switch (ft)
            {
                case FormatType.Short:
                    return server.type;
                case FormatType.Full:
                    return GetStringOfType(server.type);
                default:
                    return null;
            }
        }
        private string GetStringOfType(string type)
        {
            switch (type)
            {
                case "official":
                    return "RU24";
                case "plug-in":
                    return "Подключенный";     
                case "integrated":
                    return "Интегрированный";
                case "useradded":
                    return "Пользовательский"; 
                default:
                    return null;
            }
        }

        public string registerlink
        {
            get
            {
                if (server.type == "plug-in")
                {
                    return server.registerLink;

                }
                else
                {
                    return null;
                }
            }
        }

        public String name
        {
            get
            {
                return server.name;
            }
            set
            {

            }
        }
        public String filetyp
        {
            get
            {
                return server.filetyp;
            }
            set
            {

            }
        }

        public enum FormatType
        {
            Short,
            Full
        }
    }
    
}
