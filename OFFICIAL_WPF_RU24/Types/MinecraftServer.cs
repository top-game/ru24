
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ServerPinger
{
	public static class MinecraftServer
	{
		public static Thread ServerWatch;

		public static string[] ByteSplit(byte[] input, byte split)
		{
			string str = Encoding.BigEndianUnicode.GetString(input);
			return str.Split(new char[] { (char)split });
		}

		public static PingResponse Check(string ip, int port)
		{
			PingResponse pingResponse;
			TcpClient tcpClient = new TcpClient();
			try
			{
				tcpClient.Connect(ip, port);
			}
			catch
			{
				pingResponse = new PingResponse(false);
				return pingResponse;
			}
			tcpClient.GetStream().WriteByte(254);
			if (tcpClient.GetStream().ReadByte() == 255)
			{
				tcpClient.GetStream().ReadByte();
				tcpClient.GetStream().ReadByte();
				byte[] numArray = new byte[128];
				int num = tcpClient.GetStream().Read(numArray, 0, (int)numArray.Length);
				Array.Resize<byte>(ref numArray, num);
				string[] strArrays = MinecraftServer.ByteSplit(numArray, 167);
				pingResponse = ((int)strArrays.Length == 3 ? new PingResponse(strArrays[0], strArrays[2], strArrays[1]) : new PingResponse(false));
			}
			else
			{
				pingResponse = new PingResponse(false);
			}
			return pingResponse;
		}

		private static void ServerWatchWorker(string ip, int port)
		{
			while (true)
			{
				Thread.Sleep(5000);
				if (MinecraftServer.ServerWatchTickUpdate != null)
				{
					MinecraftServer.ServerWatchTickUpdate(MinecraftServer.Check(ip, port));
				}
			}
		}

		public static void StopWatching()
		{
			if (MinecraftServer.ServerWatch != null)
			{
				if (MinecraftServer.ServerWatch.IsAlive)
				{
					MinecraftServer.ServerWatch.Abort();
				}
			}
		}

		private static byte[] SubByte(byte[] input, int index)
		{
			byte[] numArray = new byte[(int)input.Length - index];
			Array.Copy(input, index, numArray, 0, (int)numArray.Length);
			return numArray;
		}

		public static void WatchServer(string ip, int port)
		{
			MinecraftServer.StopWatching();
			MinecraftServer.ServerWatch = new Thread(() => MinecraftServer.ServerWatchWorker(ip, port));
			MinecraftServer.ServerWatch.Start();
		}

		public static event MinecraftServer.ServerWatchTick ServerWatchTickUpdate;

		public delegate void ServerWatchTick(PingResponse pr);
	}
}