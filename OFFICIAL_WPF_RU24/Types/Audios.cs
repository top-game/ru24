﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OFFICIAL_WPF_RU24.Types
{
    public struct Audios
    {
        public Audio[] response { get; set; }
        
    }
    public struct Audio
    {
        
        public int id { get; set; }
        public string title { get; set; }
        public string artist { get; set; }
        public int duration { get; set; }
        public int lyrics_id { get; set; }

        public string url { get; set; }

        public String Composition
        {
            get
            {
                return artist + " - " + title;
            }
        }

        public String Duration
        {
            get
            {
               // DateTime dt = new DateTime()
                TimeSpan ts = TimeSpan.FromSeconds(duration);
                return ts.ToString();
            }
        }

        
    }
    
}
