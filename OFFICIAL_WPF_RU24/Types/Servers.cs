﻿using Newtonsoft.Json;
using RU24ServerLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace OFFICIAL_WPF_RU24.Types
{
    public struct Servers
    {
        public Server[] response { get; set; }
    }
    public struct Server
    {

        public IServer Lib;
        /* Start name */

        //string ip, port;

        /// <summary>
        /// Имя
        /// </summary>
        public String Name
        {
            get
            {
                if (Lib != null)
                {
                    return Lib.ServerInfo.name;
                }else
                return name;

            }
            set
            {
                name = value;
            }
        }
        public string name;
        /* End name */

        public bool ofc;

        /* Start download url */
        public string downUrl;
        /* End download url */

        /* Start register url */
        public string registerLink;
        /* End register url */

        /* Start type */
        public String Type
        {
            get
            {
                string typ = "";
                if (type == "oth") typ = "Подключаемый";
                if (type == "gth") typ = "Интегрированный";
                if (type == "igt") typ = "RU24";
                //if (type != "oth" && type != "gth") typ = "Неизвестно";
                if (Lib != null)
                {
                    typ = "Добавлен";
                }
                else
                {
                    typ = "Неизвестно";
                }
                return typ;
            }
        }
        public string type;
        /* End type */

        /* Start file type */
        public string filetyp;
        /* End file type */

        public string game;

        public int CurrentPlayers;
        public Double ProgCurrPlayers
        {
            get
            {
                return CurrentPlayers;
            }
            set
            {

            }
        }
        public int MaximumPlayers;
        public Double ProgMaxPlayers
        {
            get
            {
                if (Lib != null) { return Lib.ServerInfo.players_max; } else
                return MaximumPlayers;
            }
            set
            {

            }
        }
        
        public String Players
        {
            get
            {
                return CurrentPlayers + "/" + MaximumPlayers;
            }
        }

        public BitmapImage TypeImage
        {
            get
            {
                switch (type)
                {
                    case "con":
                        return new BitmapImage(new Uri("pack://application:,,,/OFFICIAL_WPF_RU24;component/UI/Images/erorserv.png"));
                    case "oth":
                        return new BitmapImage(new Uri("pack://application:,,,/OFFICIAL_WPF_RU24;component/UI/Images/conserv.png"));
                    
                }
                //case "igt":
                //        return new BitmapImage(new Uri("pack://application:,,,/OFFICIAL_WPF_RU24;component/UI/Images/usadserv.png"));
                //
                if (Lib != null)
                {
                    return new BitmapImage(new Uri("pack://application:,,,/OFFICIAL_WPF_RU24;component/UI/Images/usadserv.png"));
                }
                else
                {
                    return new BitmapImage(new Uri("pack://application:,,,/OFFICIAL_WPF_RU24;component/UI/Images/erorserv.png"));
                }
            }
        }

        public void InitialServer(string host)
        {
            if (this.type != "oth")
            {
                string ans = (new WebClient()).DownloadString(host + "");
                if (ans != "null") { 
                    JsonConvert.DeserializeObject<CleanServer>(ans);
                    
                }
            }
        }

        

        

        public struct CleanServer{
			///
			///
			///{
			//  "server": {
			//    "hostname": "78.46.123.8",
			//    "version": "cauldron,craftbukkit,mcpc,fml,forge 1.7.10",
			//    "onlplayers": 8,
			//    "maxplayers": 50,
			//    "motd": "Sample"
			//  }
			//}
			public string hostname;
			public string version;
			public string motd;
			public int onlplayers;
			public int maxplayers;
            
        }
    }
}
