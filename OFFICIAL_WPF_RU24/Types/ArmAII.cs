﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;

namespace OFFICIAL_WPF_RU24.Types
{
    public struct ArmAII
    {
        public static string GetArmAIIFolder()
        {
            if (Environment.Is64BitOperatingSystem)
            {
                string ans = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Bohemia Interactive Studio\\ArmA 2\\", "MAIN", "null");
                if (ans == "null") return ""; else return ans;
            }
            else
            {
                //\Software\Bohemia Interactive Studio
                string ans = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA 2\\", "MAIN", "null");
                if (ans == "null") return ""; else return ans;
            }
        }

        private string param;

        private string exefile;

        private ProcessStartInfo proc;

        /// <summary>
        /// Подготовка игры
        /// </summary>
        /// <param name="settings">Настройки игры</param>
        public void Init(Settings_ArmAII settings)
        {
            if (!Directory.Exists(settings.ArmAIIPath)) throw new InvalidDirectoryException("Неверная директория игры");
            if (!Directory.Exists(settings.ArmAIIPath + "\\Expansion\\beta\\")) throw new BetaPatchException("Бета патч не установлен, либо неверно помещен в корне папки игры!");
            if (!File.Exists(settings.ArmAIIPath + "\\Expansion\\beta\\arma2oa.exe")) throw new BetaPatchException("Бета патч не установлен, либо неверно помещен в корне папки игры!");

            proc = new ProcessStartInfo
            {
                Arguments = settings.RunParams,
                FileName = settings.ExE,
                WorkingDirectory = settings.ArmAIIPath
            };
        }

        
    }
    public struct Settings_ArmAII
    {
        public string RunParams;
        public string ExE;
        public string ArmAIIPath;
        

    }

    [Serializable]
    public class InvalidDirectoryException : Exception
    {
        public InvalidDirectoryException() { }
        public InvalidDirectoryException(string message) : base(message) { }
        public InvalidDirectoryException(string message, Exception inner) : base(message, inner) { }
        protected InvalidDirectoryException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class BetaPatchException : Exception
    {
        public BetaPatchException() { }
        public BetaPatchException(string message) : base(message) { }
        public BetaPatchException(string message, Exception inner) : base(message, inner) { }
        protected BetaPatchException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}