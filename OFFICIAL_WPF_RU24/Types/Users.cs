﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OFFICIAL_WPF_RU24.Types
{
    public struct Users
    {
        public User[] response { get; set; }
    }
    public struct User
    {
        public int uid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public City city { get; set; }
    }

    public struct City
    {
        public int id { get; set; }
        public string title { get; set; }
    }
    public struct Citys
    {
        public City[] response { get; set; }
    }
}
