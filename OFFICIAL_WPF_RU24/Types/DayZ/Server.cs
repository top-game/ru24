﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OFFICIAL_WPF_RU24.Types.DayZ
{
    public class Server
    {
        public string name { get; set; }
        public string ip { get; set; }
        public int port { get; set; }
        public string configurl { get; set; }
        
    }
}
