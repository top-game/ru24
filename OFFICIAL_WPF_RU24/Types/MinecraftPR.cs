using System;
using System.Runtime.CompilerServices;

namespace ServerPinger
{
	public class PingResponse
	{
		public bool IsOnline
		{
			get;
			set;
		}

		public string MaxPlayers
		{
			get;
			set;
		}

		public string MOTD
		{
			get;
			set;
		}

		public string OnlineUsers
		{
			get;
			set;
		}

		public PingResponse(bool online = false)
		{
			if (online)
			{
				throw new Exception(".....");
			}
			this.IsOnline = online;
			string str = "-";
			string str1 = str;
			this.OnlineUsers = str;
			string str2 = str1;
			str1 = str2;
			this.MaxPlayers = str2;
			this.MOTD = str1;
		}

		public PingResponse(string motd, string maxplayers, string onlineusers)
		{
			this.MOTD = motd;
			this.MaxPlayers = maxplayers;
			this.OnlineUsers = onlineusers;
			this.IsOnline = true;
		}
	}
}