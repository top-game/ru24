﻿using Microsoft.Win32;
using System;
using System.Linq;
namespace OFFICIAL_WPF_RU24.Types
{
    /// <summary>
    /// Представляет собой основу для запуска ArmA II с модами
    /// </summary>
    public struct ArmAIIOA
    {
        //string gamefolder { get; set; }
        //string arguments { get; set; }
        

        public static string GetArmAIIOAFolder()
        {
            if(Environment.Is64BitOperatingSystem){
                string ans = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Wow6432Node\\Bohemia Interactive Studio\\ArmA 2 OA\\","MAIN", "null");
                if (ans == "null") return ""; else return ans;
            }
            else
            {
                //\Software\Bohemia Interactive Studio
                string ans = (string)Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Bohemia Interactive Studio\\ArmA 2 OA\\", "MAIN", "null");
                if (ans == "null") return ""; else return ans;
            }
            
        }
    }
}
