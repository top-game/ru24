﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OFFICIAL_WPF_RU24.Types
{
    public static class GlobalBuffer
    {
        /// <summary>
        /// Устанавливает текущего пользователя
        /// </summary>
        public static CurrentUser User { get; set; }

        /// <summary>
        /// Токен
        /// </summary>
        public static String Token { get; set; }
    }
}
