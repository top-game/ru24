﻿using Awesomium.Core;
using Lastfm.Services;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Newtonsoft.Json;
using OFFICIAL_WPF_RU24.Properties;
using OFFICIAL_WPF_RU24.Types;
using RU24ServerLib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml;
using VKAPI;

namespace OFFICIAL_WPF_RU24
{
    /// <summary>
    /// Главное окно
    /// </summary>
    //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔═╗╔═╗
    //  ║║║║ ║ ║╣ ╠╦╝╠╣ ╠═╣║  ║╣
    //  ╩╝╚╝ ╩ ╚═╝╩╚═╚  ╩ ╩╚═╝╚═╝
    public partial class MainWindow : MetroWindow
    {
        //  ╔╦╗╔═╗╔═╗╔═╗╦ ╦╦ ╔╦╗  ╔╦╗╔═╗╔╦╗╦ ╦╔═╗╔╦╗╔═╗
        //   ║║║╣ ╠╣ ╠═╣║ ║║  ║   ║║║║╣  ║ ╠═╣║ ║ ║║╚═╗
        //  ═╩╝╚═╝╚  ╩ ╩╚═╝╩═╝╩   ╩ ╩╚═╝ ╩ ╩ ╩╚═╝═╩╝╚═╝
        public MainWindow()
        {
            this.InitializeComponent();
            App.mw = this;
            if (Settings.Default.CompSet == null)
            {
                Settings.Default.CompSet = new RU24Lib.Types.Extended.ComponentsSettings
                {
                    EnableMusic = true,
                    EnableVK = true
                };
            }
            if (Settings.Default.MeUsSet == null)
            {
                Settings.Default.MeUsSet = new RU24Lib.Types.Extended.MemoryUsageSettings
                {
                    LoadMessages = true,
                    LoadMusic = true,
                    SkipEventsLoading = false
                };
            }
            Settings.Default.Save();

            /* INIT */

            this.Flyout_Info.Position = Position.Top;
            this.Flyout_Info.Height = 75;
            this.Flyout_Info.Background = Brushes.DarkCyan;
            this.Flyout_Info.Foreground = Brushes.White;
            this.Flyout_Info.Header = "Добро пожаловать в бета-версию RU24";
            this.Flyout_Info.Content = this.Info_Label;

            this.Title = "БЕТА-ТЕСТ | О всех багах сообщайте ВКонтакте";
            this.ShowInTaskbar = true;
            //this.Opacity = 0;

            this.Closing += delegate
            {
                Environment.Exit(0);
            };
            /* END INIT */

            this.Browser.WebSession = WebCore.CreateWebSession(
                new WebPreferences()
                {
                    SmoothScrolling = true,
                    DefaultEncoding = "UTF-8",
                    EnableGPUAcceleration = true
                });

            if (OFFICIAL_WPF_RU24.Properties.Settings.Default.token != "")
            {
                this.AuthVKbyToken(OFFICIAL_WPF_RU24.Properties.Settings.Default.token);
            }

            this.trackvisenabled.IsChecked = Settings.Default.trackMe;
        }

        public void LoadEvents()
        {
            this.UserPanel.UserExitButton.Click += this.UserPanel_Exit;
            this.UserPanel.UserStatus.MouseDoubleClick += this.UserPanel_ChangeStatus;
        }

        private async void LoadForm()
        {
            if (!this.isLoaded)
            {
                App.lf.Write("MainWindow", "Загрузка главного окна - начало...");
                if (OFFICIAL_WPF_RU24.Properties.Settings.Default.FirstStart)
                {
                    this.ShowInformation("Добро пожаловать, это первый раз когда Вы запустили эту программу!");
                    App.lf.Write("INFO", "Пользователь впервые запускает программу");
                }
                OFFICIAL_WPF_RU24.Properties.Settings.Default.FirstStart = false;
                OFFICIAL_WPF_RU24.Properties.Settings.Default.Save();

                await Task.Factory.StartNew(() => this.ConnectToServer());
                if (this.isOffline)
                {
                    await this.ShowMessageAsync("Ответ отрицательный!", "Сервер не отвечает, возможно недоступен. Было предпринято 3 попытки соеденения");
                    App.lf.Write("WARNING", "Возникли проблемы с соединением к главному серверу TOP-GAME");
                }
                
                this.isLoaded = true;
            }

            if (!Settings.Default.CompSet.EnableVK)
            {
                this.skipvk = true;
                App.lf.Write("VK/INFO", "ВКонтакте выключен, произойдет пропуск");
            }

            if (this.skipvk)
            {
                App.lf.Write("VK/INFO", "Пропуск авторизационного процесса ВКонтакте");
                return;
            }
            if (!this.isbytoken)
            {
                var VK = new VkAuthorization();
                VK.Show();
                VKAPI.Auth.VK vko = await VK.Get();
                VK.Hide();
                if (vko == null)
                {
                    this.ShowInformation("Ошибка авторизации");
                    App.lf.Write("VK/ERROR", "Пользователь разорвал попытку авторизации");
                    return;
                }
            }

            try
            {
                this.UserPanel.UserAvatar.Source = new BitmapImage(new Uri(VK_DATA.GetData()[4].Value, UriKind.Absolute));
                App.lf.Write("VK/INFO", string.Format("Пользовательское изображение загруженно по ссылке: {0}", VK_DATA.GetData()[4].Value));
            }
            catch (Exception)
            {
                Task.Factory.StartNew(delegate
                {
                    Thread.Sleep(5000);
                    Settings.Default.token = "";
                    OFFICIAL_WPF_RU24.Properties.Settings.Default.Save();
                    Process.Start(Application.ResourceAssembly.Location);
                    this.Dispatcher.Invoke(delegate { Application.Current.Shutdown(); });
                });
                MessageBox.Show("Требуется обнуление токена ВКонтакте.\nТокен будет сброшен через 5 секунд, программа перезагрузится!\nПожалуйста сохраняйте спокойствие!");
                this.isbytoken = false;
                this.isAuthed = false;
                this.LoadForm();
                return;
            }

            string req = string.Format("https://api.vk.com/method/status.get.xml?user_id={0}&access_token={1}", VK_DATA.GetData()[0].Value, VK_DATA.Token);
            string ans = VK_DATA.Request(req);

            ans = DaFApi.Utilites.GetBetween(ans, "<text>", "</text>");
            if (ans == "")
            {
                ans = "Пустой статус :(";
            }
            this.UserPanel.UserStatus.Content = ans;

            string userid = VKAPI.VK_DATA.GetData()[0].Value;
            var xml = new XmlDocument();
            xml.Load(string.Format("https://api.vk.com/method/groups.isMember.xml?token{0}&user_id={1}&group_id=66292960&extended=1", VKAPI.VK_DATA.Token, userid));

            this.userNameTextBlock1.Text = VKAPI.VK_DATA.GetData()[1].Value;
            this.UserPanel.UserName.Content = string.Format("{0} {1}", VK_DATA.GetData()[1].Value, VK_DATA.GetData()[2].Value);
            if (DaFApi.Utilites.GetBetween(xml.InnerXml, "<member>", "</member>") == "0")
            {
                var btn = new Button();
                btn.Content = "Вступить";
                var stk = new StackPanel();
                stk.Children.Add(btn);

                btn.Click += delegate
                {
                    VK_DATA.Request(string.Format("https://api.vk.com/method/groups.join?group_id=66292960&access_token={0}", VK_DATA.Token));
                    this.ShowInformation(string.Format("Теперь ты с нами в группе! Добро пожаловать, {0}", VK_DATA.GetData()[1].Value));
                };

                this.ShowInformation(string.Format("{0}, вступай в группу! Будь в курсе новостей ;)", VK_DATA.GetData()[1].Value), stk);
            }
            else
            {
                //Убрал - мешает
                //ShowInformation(VKAPI.VK_DATA.GetData()[1].Value + ", вы в нашей группе ВКонтакте. Спасибо :3");
            }

            //Методы проверок

            //GetMessages();
            this.TrackVisitor();
            //Добавил один поток для обновлений сообщений
            if (Settings.Default.CompSet.EnableVK)
            {
                if (Settings.Default.CompSet.EnableMusic)
                {
                    this.GetMusic();
                }
                if (Settings.Default.MeUsSet.LoadMessages)
                {
                    Task.Factory.StartNew(delegate
                    {
                        try
                        {
                            do
                            {
                                Thread.Sleep(500);

                                this.GetMessages();
                            }
                            while (true);
                        }
                        catch (Exception)
                        {
                            return;
                        }
                    });
                }
            }
            //});

            //Убрал - ненужно. Только память ест
            //GlobalBuffer.User = new CurrentUser(VKAPI.VK_DATA.GetData()[1].Value, VKAPI.VK_DATA.GetData()[2].Value, new System.Uri(VKAPI.VK_DATA.GetData()[4].Value)) { UID = Convert.ToInt32(userid) };
            //GlobalBuffer.Token = VKAPI.VK_DATA.Token;

            //Загрузка ивентов
            this.LoadEvents();
            //Загрузка плеера
            this.Init();

            OFFICIAL_WPF_RU24.Properties.Settings.Default.token = VK_DATA.Token;
            OFFICIAL_WPF_RU24.Properties.Settings.Default.Save();

            //Сообщает что авторизация завершена.
            this.isAuthed = true;
        }

        //  ╦  ╦╔═╗╦═╗╔═╗
        //  ╚╗╔╝╠═╣╠╦╝╚═╗
        //   ╚╝ ╩ ╩╩╚═╚═╝

        #region Vars

        private bool isAuthed = false;
        private bool isbytoken = false;
        private bool isLoaded = false;
        private bool isOffline = false;
        private Lastfm.Services.Session lastfmsession = new Lastfm.Services.Session("91c8d4b12cae6c672fbcff5e797dca7c", "ac4766eab85395c46677e5df3bdba521");
        //private bool SelectionEnded = false;

        private Server[] serverZ = null;

        private bool skipvk = false;

        #endregion Vars

        //  ╔═╗╔═╗╦═╗╦  ╦╦╔═╗╔═╗╔═╗
        //  ╚═╗║╣ ╠╦╝╚╗╔╝║║  ║╣ ╚═╗
        //  ╚═╝╚═╝╩╚═ ╚╝ ╩╚═╝╚═╝╚═╝

        #region Services

        public async void ConnectToServer()
        {
            int tryNum = 0;

        trythis:
            try
            {
                PingReply ping = await (new Ping()).SendPingAsync("http://top-game.url.ph/");
                //ping.Send("http://top-game.url.ph/");
                if (ping.Status == IPStatus.Success)
                {
                    this.isOffline = false;
                }
                else
                {
                    this.isOffline = true;
                }
            }
            catch (Exception)
            {
                if (tryNum < 4)
                {
                    Thread.Sleep(500);
                    tryNum++;
                    goto trythis;
                }
                else
                {
                    this.isOffline = true;
                }
            }
        }

        private void AuthVKbyToken(string token)
        {
            var vk = new Auth.VK(new VKAPI.TYPES.Token(token));
            this.isLoaded = true;
            this.isbytoken = true;
            this.LoadForm();
            this.isLoaded = false;
            this.isAuthed = true;
            this.skipvk = true;
        }

        private void GetMessages()
        {
            string req, ans;
            req = string.Format("https://api.vk.com/method/messages.getDialogs?unread=1&v=5.28&access_token={0}", VK_DATA.Token);
            ans = VK_DATA.Request(req);
            //MessageBox.Show(ans);
            dynamic jsonconverted = JsonConvert.DeserializeObject(ans);

            if (jsonconverted.response.count != "0")
            {
                string newmescou = "0";
                try
                {
                    int _1nmc = Convert.ToInt32(jsonconverted.response.count);
                    if (_1nmc > 9)
                    {
                        newmescou = "+";
                    }
                    else
                    {
                        newmescou = jsonconverted.response.count;
                    }
                }
                catch
                {
                    return;
                }
                this.Dispatcher.Invoke(delegate
                {
                    this.newNotifyIcon.Text = "*";
                    this.newNotifyCounter.Visibility = System.Windows.Visibility.Visible;
                    this.newNotifyCountertb.Text = newmescou;
                });
            }
            else
            {
                this.Dispatcher.Invoke(delegate
                {
                    this.newNotifyCounter.Visibility = System.Windows.Visibility.Hidden;
                });
            }
        }

        private void GetMusic()
        {
            string req, ans;
            req = string.Format("https://api.vk.com/method/audio.get?user_id={0}&access_token={1}", VK_DATA.GetData()[0].Value, VK_DATA.Token);
            ans = VK_DATA.Request(req);

            Audios audiosofauser = JsonConvert.DeserializeObject<Audios>(ans);
            this.MusicGrid.ItemsSource = audiosofauser.response;
        }

        #endregion Services

        //  ╔╦╗╔═╗╔╦╗╦╔═╗
        //  ║║║║╣  ║║║╠═╣
        //  ╩ ╩╚═╝═╩╝╩╩ ╩

        #region Media

        /// <summary>
        /// Проверяет инициализацию
        /// Истинно, если пройдена. Иначе ложно
        /// </summary>
        private bool isInited = false;

        /// <summary>
        /// [КЭШ-ОБЪЕКТ] Максимальное время воспроизведения композиции
        /// </summary>
        private string max;

        /// <summary>
        /// Сам плеер
        /// </summary>
        private MediaPlayer mediaplayer = new MediaPlayer();

        /// <summary>
        /// Таймер музыкального плеера.
        /// Очень слабая нервная система - не рекомендуется трогать
        /// </summary>
        private System.Windows.Forms.Timer mediatimer = new System.Windows.Forms.Timer();

        /// <summary>
        /// [КЭШ-ОБЪЕКТ] Изображение альбома
        /// </summary>
        private BitmapImage Picture = new BitmapImage();

        /// <summary>
        /// Состояние проигрывания
        /// Истинно, если проигрывается. Иначе ложно
        /// </summary>
        private bool PlayState = false;

        /// <summary>
        /// Инициализация плеера
        /// ОБЯЗАТЕЛЬНО ПРИ ЗАГРУЗКЕ!
        /// </summary>
        private void Init()
        {
            //mediatimer.Enabled = true;
            this.CurrentMusic_Time.Content = string.Format("{0}/{1}", this.mediaplayer.Position.ToString(), this.max);

            this.mediaplayer = new MediaPlayer();
            this.mediaplayer.Changed += this.mediaplayer_Changed;
            this.mediatimer.Tick += this.mediatimer_Tick;
        }

        /// <summary>
        /// Инициализация инвентов
        /// Если хотите видеть время
        /// </summary>
        private void InitEvents()
        {
            this.isInited = true;
            //Task.Factory.StartNew(delegate
            //{
            //    try
            //    {
            //        do
            //        {
            //            Thread.Sleep(500);
            //            Dispatcher.Invoke(delegate
            //            {
            //                CurrentMusic_Time.Content = mediaplayer.Position.ToString(@"mm\:ss") + "/" + max + "  [" + mediaplayer.DownloadProgress * 100 + "%]";
            //            });
            //        } while (true);
            //    }
            //    catch (Exception)
            //    {
            //        return;
            //    }
            //});
        }

        private void LoadImage(string fname)
        {
            try
            {
                if (!Directory.Exists(string.Format("{0}\\albumarts\\", Environment.CurrentDirectory)))
                {
                    Directory.CreateDirectory(string.Format("{0}\\albumarts\\", Environment.CurrentDirectory));
                }
                string completed = string.Format("{0}\\albumarts\\{1}.jpg", Environment.CurrentDirectory, DateTime.Now.Ticks);
                var wc = new WebClient();
                wc.DownloadFileCompleted += delegate
                {
                    this.Dispatcher.Invoke(delegate { this.CurrentMusic_Image.Source = new BitmapImage(new Uri(completed)); });
                };
                wc.DownloadFileAsync(new Uri(fname), completed);
            }
            catch
            {
                Console.WriteLine("Ошибка загрузки изображения");
            }
        }

        private void mediaplayer_Changed(object sender, EventArgs e)
        {
            this.CurrentMusic_Time.Content = string.Format("{0}{1}{2} [{3}%]", this.mediaplayer.Position.ToString(@"mm\:ss"), @"\", this.max, this.mediaplayer.BufferingProgress);
        }

        private void mediatimer_Tick(object sender, EventArgs e)
        {
            //Console.WriteLine("timer - tick!");
            this.mediaplayer_Changed(sender, e);
        }

        /// <summary>
        /// Открывает аудио
        /// </summary>
        /// <param name="audio">Аудиофайл из ВКонтакте</param>
        private void Open(Audio audio)
        {
            this.CurrentMusic_Composition.Text = audio.Composition;
            this.max = audio.Duration;
            this.mediaplayer.Open(new Uri(audio.url));
            Task.Factory.StartNew(delegate
            {
                var ts = new Lastfm.Services.TrackSearch(audio.Composition, this.lastfmsession);
                try
                {
                    Track track = ts.GetFirstMatch();
                    Album alb = track.GetAlbum();

                    this.LoadImage(alb.GetImageURL());
                }
                catch (Exception)
                {
                    Console.WriteLine("Ошибка загрузки изображения. Источник не найден!");
                    this.Dispatcher.Invoke(delegate
                    {
                        this.CurrentMusic_Image.Source = new BitmapImage(new Uri("pack://application:,,,/OFFICIAL_WPF_RU24;component/UI/Images/music.png"));
                    });
                }
            });
        }

        /// <summary>
        /// Пауза воспроизведения
        /// </summary>
        private void Pause()
        {
            this.mediaplayer.Pause();
            this.mediatimer.Stop();
            this.PlayState = false;
        }

        /// <summary>
        /// Продолжить/Начать воспроизведение
        /// </summary>
        private void Play()
        {
            this.mediaplayer.Play();
            this.mediatimer.Start();
            this.PlayState = true;
            if (!this.isInited)
            {
                this.InitEvents();
            }
        }

        /// <summary>
        /// Останавливает воспроизведение
        /// </summary>
        private void Stop()
        {
            this.mediaplayer.Stop();
            this.mediatimer.Stop();
            this.PlayState = false;
        }
        #endregion Media

        //  ╦╔╗╔╔═╗╔═╗╦═╗╔╦╗╔═╗╔╦╗╦╔═╗╔╗╔
        //  ║║║║╠╣ ║ ║╠╦╝║║║╠═╣ ║ ║║ ║║║║
        //  ╩╝╚╝╚  ╚═╝╩╚═╩ ╩╩ ╩ ╩ ╩╚═╝╝╚╝
        //  ╔═╗╔╗╔╔╦╗  ╦ ╦╔═╗╦═╗╔╗╔╦╔╗╔╔═╗╔═╗
        //  ╠═╣║║║ ║║  ║║║╠═╣╠╦╝║║║║║║║║ ╦╚═╗
        //  ╩ ╩╝╚╝═╩╝  ╚╩╝╩ ╩╩╚═╝╚╝╩╝╚╝╚═╝╚═╝

        #region Inf&Warns

        /// <summary>
        /// Показывает информацию, содержащую только текст
        /// </summary>
        /// <param name="text">Текст</param>
        private void ShowInformation(string text)
        {
            this.Flyout_Info.IsOpen = false;
            this.Flyout_Info.Content = null;
            this.Flyout_Info.Background = Brushes.DarkCyan;
            this.Flyout_Info.Header = text;
            this.Info_Label.Content = "";
            this.Flyout_Info.IsOpen = true;
        }

        /// <summary>
        /// Показывает информацию, содержащую заголовок и описание
        /// </summary>
        /// <param name="title">Заголовок</param>
        /// <param name="description">Описание</param>
        private void ShowInformation(string title, string description)
        {
            this.Flyout_Info.IsOpen = false;
            this.Flyout_Info.Content = null;
            this.Flyout_Info.Header = title;
            this.Info_Label.Content = description;
            this.Flyout_Info.IsOpen = true;
        }

        /// <summary>
        /// Показывает информацию, содержащую заголовок и объект, помещенный в StackPanel
        /// </summary>
        /// <param name="title">Заголовок</param>
        /// <param name="ctrl">Объект, помещенный в StackPanel</param>
        private void ShowInformation(string title, StackPanel ctrl)
        {
            this.Flyout_Info.IsOpen = false;
            this.Flyout_Info.Content = null;
            this.Flyout_Info.Header = title;
            this.Flyout_Info.Content = ctrl;
            this.Flyout_Info.IsOpen = true;
        }

        /// <summary>
        /// Показывает информацию, ожидая пока пользователь закроет её.
        /// Содержит заголовок и описание
        /// </summary>
        /// <param name="title">Заголовок</param>
        /// <param name="description">Описание</param>
        [Obsolete("Нарушает async порядок! Плохая оптимизация!", true)]
        private void ShowInformationAsync(string title, string description)
        {
            this.Dispatcher.Invoke(delegate
            {
                this.Flyout_Info.IsOpen = false;
                this.Flyout_Info.Content = null;
                this.Flyout_Info.Header = title;
                this.Info_Label.Content = description;
                this.Flyout_Info.IsOpen = true;
            });
        }

        /// <summary>
        /// Показывает предупреждение, содержащее только текст
        /// </summary>
        /// <param name="text">Текст</param>
        private void ShowWarning(string text)
        {
            this.Flyout_Info.IsOpen = false;
            this.Flyout_Info.Content = null;
            this.Flyout_Info.Header = text;
            this.Flyout_Info.Background = Brushes.DarkRed;
            this.Flyout_Info.Foreground = Brushes.White;
            this.Info_Label.Foreground = Brushes.White;
            this.Flyout_Info.IsOpen = true;
        }


        public enum ResponseType
        {
            Yes,
            No,
            Cancel,
            OtherButton,
            NoResponse
        }

        public enum DialogStyle{
            Yes,
            YesNo,
            YesNoCancel,
            YesNoOther
        }

        public class DialogSettings
        {
            public string YesButtonText = "Да";
            public string NoButtonText = "Нет";
            public string CancelButtonText = "Отмена";
            public string OthetButtonText = "Другое";

        }

        public async Task<ResponseType> ShowDialogWindow(string title, string message, DialogStyle style = DialogStyle.Yes, DialogSettings settings = null)
        {
            MessageDialogStyle msgs;

            switch (style)
            {
                case DialogStyle.YesNo:
                    msgs = MessageDialogStyle.AffirmativeAndNegative;
                    break;
                case DialogStyle.YesNoCancel:
                    msgs = MessageDialogStyle.AffirmativeAndNegativeAndDoubleAuxiliary;
                    break;
                case DialogStyle.YesNoOther:
                    msgs = MessageDialogStyle.AffirmativeAndNegativeAndSingleAuxiliary;
                    break;
                default:
                    msgs = MessageDialogStyle.AffirmativeAndNegative;
                    break;
            }

            if (settings == null)
            {
                settings = new DialogSettings()
                {
                    YesButtonText = "Хорошо",
                    CancelButtonText = "Отмена",
                    NoButtonText = "Нет",
                    OthetButtonText = "Что-то другое"
                };
            }
            MessageDialogResult mdr = MessageDialogResult.Affirmative;
            try
            {
                mdr = await this.ShowMessageAsync(title, message, msgs, new MetroDialogSettings
                {
                    ColorScheme = MetroDialogColorScheme.Theme,
                    AffirmativeButtonText = settings.YesButtonText,
                    NegativeButtonText = settings.NoButtonText,
                    FirstAuxiliaryButtonText = settings.CancelButtonText,
                    SecondAuxiliaryButtonText = settings.OthetButtonText
                });
            }
            catch (Exception)
            {
                Dispatcher.Invoke(async delegate
                {
                    mdr = await this.ShowMessageAsync(title, message, msgs, new MetroDialogSettings
                    {
                        ColorScheme = MetroDialogColorScheme.Theme,
                        AffirmativeButtonText = settings.YesButtonText,
                        NegativeButtonText = settings.NoButtonText,
                        FirstAuxiliaryButtonText = settings.CancelButtonText,
                        SecondAuxiliaryButtonText = settings.OthetButtonText
                    });
                });
            }
            

            switch (mdr)
            {
                case MessageDialogResult.Affirmative:
                    return ResponseType.Yes;

                case MessageDialogResult.Negative:
                    return ResponseType.No;

                case MessageDialogResult.FirstAuxiliary:
                    return ResponseType.Cancel;

                case MessageDialogResult.SecondAuxiliary:
                    return ResponseType.OtherButton;

                default:
                    return ResponseType.NoResponse;
            }
            
        }

        

        #endregion Inf&Warns

        //  ╔═╗╦  ╦╔═╗╔╗╔╔╦╗╔═╗
        //  ║╣ ╚╗╔╝║╣ ║║║ ║ ╚═╗
        //  ╚═╝ ╚╝ ╚═╝╝╚╝ ╩ ╚═╝
        //  ╔═╗╔═╗  ╔═╗╔╗  ╦╔═╗╔═╗╔╦╗╔═╗
        //  ║ ║╠╣   ║ ║╠╩╗ ║║╣ ║   ║ ╚═╗
        //  ╚═╝╚    ╚═╝╚═╝╚╝╚═╝╚═╝ ╩ ╚═╝

        #region Events

        //Библотекы
        /// <summary>
        /// Загружает сторонний сервер, не использовать c обычными серверами
        /// </summary>
        /// <param name="srvsele">Сервер</param>
        public async void LoadOtherServer(Types.Server srvsele)
        {
            if (srvsele.Lib != null)
            {
                throw new InvalidServerException("Передан сервер, который содержит подключаемую библитеку. Загрузите библотеку и загрузите её прототип.");
            }
            MessageDialogResult DR = await this.ShowMessageAsync("Подключаемый проект", "Это подключаемый проект, хотите загрузить установщик/лаунчер?", MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Theme, AffirmativeButtonText = "Скачать", NegativeButtonText = "Отмена" });
            if (DR == MessageDialogResult.Negative)
            {
                return;
            }

            //Process.Start(srvsele.downUrl);
            //return;
            ProgressDialogController pdc = await this.ShowProgressAsync("Загрузка...", "Загрузка лаунчера стороннего проекта. Это может продлится от 1 минуты до 20, в зависимости от скорости вашего интернет-соединения", false, new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Theme });
            var wc = new WebClient();
            wc.DownloadProgressChanged += new DownloadProgressChangedEventHandler(delegate(object send, DownloadProgressChangedEventArgs eventargs)
            {
                pdc.SetMessage(string.Format("Загрузка лаунчера стороннего проекта. Это может длится до 20 минут в зависимости от скорости интернета. \nУже загружено: {0}%. Это  {1}КБ\\{2}КБ", eventargs.ProgressPercentage, Math.Abs(Convert.ToInt32(eventargs.BytesReceived)) / 1024, Math.Abs(Convert.ToInt32(eventargs.TotalBytesToReceive)) / 1024));
                pdc.SetProgress(eventargs.ProgressPercentage / 100.0);
            });

            string pathtofile = string.Format("{0}\\wpf_ru24\\cached.{1}", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), srvsele.filetyp);
            string pathtofold = string.Format("{0}\\wpf_ru24\\", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments));

            wc.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(
                async delegate(object send, System.ComponentModel.AsyncCompletedEventArgs evn)
                {
                    await pdc.CloseAsync();
                    DR = await this.ShowMessageAsync("Загружено. Давайте далее!", "Что следует сделать?", MessageDialogStyle.AffirmativeAndNegativeAndDoubleAuxiliary, new MetroDialogSettings() { AffirmativeButtonText = "Открыть (возм. треб. админ-права)", NegativeButtonText = "Открыть папку", FirstAuxiliaryButtonText = "Удалить", SecondAuxiliaryButtonText = "Пропустить", ColorScheme = MetroDialogColorScheme.Theme });
                    if (DR == MessageDialogResult.Affirmative)
                    {
                        try
                        {
                            Process.Start(pathtofile);
                        }
                        catch
                        {
                            this.ShowMessageAsync("Операция была отмена", string.Format("Как сообщает Windows операция была отменена пользователем. В следстие чего произошла ошибка. Как сообщает очевидец {0} пользователь сам взял и отклонил операцию открытия файла.", (new string[3] { "Владимир", "Иван", "Петр" })[(new Random()).Next(0, 3)]));
                        }
                    }
                    else if (DR == MessageDialogResult.Negative)
                    {
                        //Process.Start(new ProcessStartInfo()
                        //{
                        //    FileName = "cmd.exe",
                        //    Arguments = "/c explorer.exe /b /select " + Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\wpf_ru24\\cached." + srvsele.filetyp,
                        //    WindowStyle = ProcessWindowStyle.Hidden
                        //});
                        Process.Start(new ProcessStartInfo()
                        {
                            FileName = string.Format("{0}\\explorer.exe", Environment.GetFolderPath(Environment.SpecialFolder.Windows)),
                            Arguments = string.Format("/select,{0}", pathtofile)
                        });
                        //MessageBox.Show("/select," + pathtofile);
                    }
                    else if (DR == MessageDialogResult.FirstAuxiliary)
                    {
                        File.Delete(pathtofile);
                    }
                    else if (DR == MessageDialogResult.SecondAuxiliary)
                    {
                    }
                });
            Directory.CreateDirectory(string.Format("{0}\\wpf_ru24\\", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)));
            wc.DownloadFileAsync(new Uri(srvsele.downUrl), string.Format("{0}\\wpf_ru24\\cached.{1}", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), srvsele.filetyp));
        }

        public async void LoadPluginServer(IServer server)
        {
            try
            {
                bool ServerWasAcceptOperations = await server.LoadRequest();
                if (!ServerWasAcceptOperations)
                {
                    //this.ShowMessageAsync("Модуль отверг загрузку!", "После отправки Request запроса серверу запрос был отвергнут, возможно вы заблокированы или модулю чего-то недостаёт!");
                    return;
                }
            }
            catch (Exception exc)
            {
                this.ShowMessageAsync("Модуль вызвал ошибку", exc.Message);
                return;
            }

            //try {
            //    server.Load();
            //}
            //catch (Exception exc)
            //{
            //    this.ShowMessageAsync("Модуль прервал загрузку!", exc.Message);
            //    return;
            //}

            try
            {
                bool ServerWasLoaded = await server.Load();
                if (ServerWasLoaded)
                {
                    //RunGame(server);
                }
            }
            catch (Exception exc)
            {
                this.ShowMessageAsync("Модуль отверг загрузку!", exc.Message);
                return;
            }
            return;
        }

        private void AutoARMAIIdir_IsCheckedChanged(object sender, EventArgs e)
        {
            if (this.AutoARMAIIdir.IsChecked.Value)
            {
                this.arma2dir.Text = ArmAII.GetArmAIIFolder();
                this.arma2dir.IsEnabled = false;
            }
            else
            {
                this.arma2dir.IsEnabled = true;
            }
        }

        private void AutoARMAIIOAdir_IsCheckedChanged(object sender, EventArgs e)
        {
            if (this.AutoARMAIIOAdir.IsChecked.Value)
            {
                this.arma2oadir.Text = ArmAIIOA.GetArmAIIOAFolder();
                this.arma2oadir.IsEnabled = false;
            }
            else
            {
                this.arma2oadir.IsEnabled = true;
            }
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.settingsFlyout.IsOpen = !this.settingsFlyout.IsOpen;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            if (!(this.MusicGrid.SelectedIndex - 1 < 0))
            {
                this.MusicGrid.SelectedIndex--;
                this.Open((Audio)this.MusicGrid.SelectedItem);
                this.Play();
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (!this.isAuthed)
            {
                this.LoadForm();
                return;
            }

            this.UserPopup.IsOpen = !this.UserPopup.IsOpen;
            //MainTabControl.SelectedIndex = MainTabControl.Items.Count-1;
            //TextRange textRange;
            //System.IO.FileStream fileStream;
            //if (System.IO.File.Exists(""))
            //{
            //    textRange = new TextRange(richTextBoxLic.Document.ContentStart, richTextBoxLic.Document.ContentEnd);
            //    using (fileStream = new System.IO.FileStream("Document.rtf", System.IO.FileMode.OpenOrCreate))
            //    {
            //        textRange.Load(fileStream, System.Windows.DataFormats.Rtf);
            //    }
            //}
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            this.MainTabControl.SelectedIndex = 1;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            this.MainTabControl.SelectedIndex = 3;
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            try
            {
                var srvsele = (Types.Server)this.Servers.SelectedItem;
                if (srvsele.type == "oth")
                {
                    this.MainTabControl.SelectedIndex = 2;
                    this.Browser.Source = new Uri(srvsele.registerLink);
                }
                else
                {
                    this.ShowMessageAsync("Не требуется", "Регистрация не требуется/Сервер не является сторонним!");
                }
            }
            catch (Exception)
            {
                this.ShowMessageAsync("Что-то явно не так!", "Возможно, сбой в преобразовании объекта, а может и нулевая ссылка. Не кликай эту кнопку и все дела, а лучше скажи администрации, тогда - спасибо!");
            }
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            this.MainTabControl.SelectedIndex = 0;
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            (new LauncherController()).ShowDialog();
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            if (this.PlayState)
            {
                this.Pause();
            }
            else
            {
                this.Play();
            }
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            if (!(this.MusicGrid.SelectedIndex + 1 > this.MusicGrid.Items.Count))
            {
                this.MusicGrid.SelectedIndex = this.MusicGrid.SelectedIndex + 1;
                this.MusicGrid_MouseDoubleClick(null, null);
            }
        }

        private void Button_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
        }

        private void CurrentMusic_HideButton_Click(object sender, RoutedEventArgs e)
        {
            this.CurrentMusic_Panel.Height = 50;
            this.CurrentMusic_Image.Visibility = System.Windows.Visibility.Hidden;
            this.CurrentMusic_Image.Height = 0;
        }

        private void Grid_Initialized(object sender, EventArgs e)
        {
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            App.Current.Shutdown(0);
        }

        private void MetroWindow_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!Settings.Default.MeUsSet.LoadMusic || !Settings.Default.CompSet.EnableMusic)
            {
                this.mti = null;
               // GC.Collect();
            }

            //Task.Factory.StartNew(delegate {
            //    double opac = 0;
            //    while (opac < 1.0)
            //    {
            //        Thread.Sleep(60);
            //        opac += 0.1;
            //        Dispatcher.Invoke(delegate
            //        {
            //            this.Opacity = opac;
            //        });
            //    }
            //});
            //Игра.IsOpen = true;
            this.HelloText.Content = this.isAuthed ? string.Format("Привет, {0}!", VK_DATA.GetData()[1].Value) : "Привет, неопознанный! :)";
            WindowState = WindowState.Normal;
        }

        private void MetroWindow_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.F1)
            {
            }
        }

        private void MetroWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.MainTabControl.Width = this.Width;
            this.MainTabControl.Height = this.Height;
        }

        private void MusicGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.MusicGrid.SelectedItem != null)
            {
                this.Open((Audio)this.MusicGrid.SelectedItem);
                this.Play();
            }
        }

        private void richTextBoxLic_Initialized(object sender, EventArgs e)
        {
            //try
            //{
            //    FileStream stream = new FileStream(Environment.CurrentDirectory + "lic.tgdoc", FileMode.Open);
            //    richTextBoxLic.Selection.Load(stream, DataFormats.Rtf);
            //}
            //catch (Exception)
            //{
            //}
        }

        private void SaveParametrsButton_Click(object sender, RoutedEventArgs e)
        {
        }

        //   #####                                      #####
        //  #     # ######   ##   #####   ####  #    # #     # ###### #####  #    # ###### #####   ####
        //  #       #       #  #  #    # #    # #    # #       #      #    # #    # #      #    # #
        //   #####  #####  #    # #    # #      ######  #####  #####  #    # #    # #####  #    #  ####
        //        # #      ###### #####  #      #    #       # #      #####  #    # #      #####       #
        //  #     # #      #    # #   #  #    # #    # #     # #      #   #   #  #  #      #   #  #    #
        //   #####  ###### #    # #    #  ####  #    #  #####  ###### #    #   ##   ###### #    #  ####
        private async void searchbtn_Click(object sender, RoutedEventArgs e)
        {
            if (!Settings.Default.CompSet.EnableVK)
            {
                if (Settings.Default.token == "")
                {
                    var vkAuth = new VkAuthorization();

                    vkAuth.Show();

                    VKAPI.Auth.VK vk = await vkAuth.Get();

                    vkAuth.Close();

                    if (vk != null)
                    {
                        Settings.Default.token = VK_DATA.Token;
                        Settings.Default.Save();
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                }
            }
            if (!this.isAuthed)
            {
                this.ShowInformation("Прежде чем искать, войдите в ВКонтакте!");
                return;
            }

            if (this.GameCombobox.SelectedItem == null)
            {
                this.ShowWarning("Нельзя искать сервера для всех игр, пока");
                return;
            }

            this.Servers.Items.Clear();
            this.searchbtn.IsEnabled = false;
            string game;
            string normalgamestring;
            switch (this.GameCombobox.SelectedIndex)
            {
                case 0:
                    game = "DayZ";
                    normalgamestring = "DayZ";
                    break;

                case 1:
                    game = "Minecraft";
                    normalgamestring = "Minecraft";
                    break;

                case 2:
                    game = "ProjZomb";
                    normalgamestring = "Project: Zomboid";
                    break;

                default:
                    return;
            }

            await Task.Factory.StartNew(delegate
            {
                string dir = string.Format("{0}\\RU24Libs\\Servers\\{1}", Environment.CurrentDirectory, game);
                if (Directory.Exists(dir))
                {
                    string[] pluginFiles = Directory.GetFiles(dir, "*.dll");
                    if (pluginFiles.Length == 0 || pluginFiles.Length == -1)
                    {
                        return;
                    }
                    IServer[] loaders = null;
                    try
                    {
                        loaders = (
                                         from file in pluginFiles
                                         let asm = Assembly.LoadFile(file)
                                         from type in asm.GetTypes()
                                         where typeof(IServer).IsAssignableFrom(type)
                                         select (IServer)Activator.CreateInstance(type)).ToArray();
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Невозможно загрузить модули, один из них некорректен, его версия, скорее всего, стара!");
                        this.ShowMessageAsync("Глобальная ошибка!", "Один из ваших модулей не обновлен, либо некорректен! Пожалуйста, обновите модули. Загрузка остальных из-за одного прерывается");
                    }
                    if(loaders != null)
                    foreach (IServer server in loaders)
                    {
                        try
                        {
                            server.Initialized();
                            this.Dispatcher.Invoke(delegate { this.Servers.Items.Add(new Types.Server { Lib = server }); });
                        }
                        catch (Exception exc)
                        {
                            Console.WriteLine(string.Format("error while loading plug : {0}", exc.Message));
                        }
                    }
                }
                else
                {
                    Directory.CreateDirectory(dir);
                }
            });

            string ans = VK_DATA.Request(string.Format("https://api.vk.com/method/execute.servers{0}?access_token={1}", game, VK_DATA.Token));

            Types.Servers srvrs = JsonConvert.DeserializeObject<Types.Servers>(ans);
            //MessageBox.Show(ans);
            if (srvrs.response == null)
            {
                this.ShowWarning(string.Format("Для {0} пока нет серверов, сожалеем :c", normalgamestring));
                this.searchbtn.IsEnabled = true;

                {
                    game = null;
                    normalgamestring = null;
                    ans = null;
                    //srvrs = null;
                }

                return; //RETURN
            }

            Server[] srv_list1 = null;
            var srv_list2 = new List<Types.Server>();

            foreach (Server serv in srvrs.response)
            {
                if (serv.name != "null")
                {
                    srv_list2.Add(serv);
                }
                else
                {
                    continue;
                }
            }

            srv_list1 = srv_list2.ToArray();
            srv_list2 = null;

            this.serverZ = null;
            this.serverZ = srv_list1;

            foreach (Server item in srv_list1)
            {
                this.Servers.Items.Add(item);
            }

            this.searchbtn.IsEnabled = true;
        }

        private async void Servers_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (this.Servers.SelectedItem == null)
            {
                return; //exit
            }
            Exception exception = null;

            try
            {
                if (((Types.Server)this.Servers.SelectedItem).Lib != null)
                {
                    IServer RealServer = ((Types.Server)this.Servers.SelectedItem).Lib;
                    this.LoadPluginServer(RealServer);
                }
                else
                {
                    if (((Types.Server)this.Servers.SelectedItem).type == "oth")
                    {
                        this.LoadOtherServer(((Types.Server)this.Servers.SelectedItem));
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ex;
            }
            if (exception != null)
            {
                MessageDialogResult que = await this.ShowMessageAsync("Хэй, друг!", "Произошла ошибка, ты бы мог помочь проекту и отправить сведения об ошибке в специальную тему, но при этом придется создать дамп. А мог бы не помогать и продолжать пользоватся программой.", MessageDialogStyle.AffirmativeAndNegative, new MetroDialogSettings() { AffirmativeButtonText = "Я выбираю помощь, создавайте дамп!", NegativeButtonText = "Я пользоватся буду, извините" });
                if (que == MessageDialogResult.Affirmative)
                {
                    throw exception;
                }
                else
                {
                    return;
                }
            }
        }

        private void Servers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void StackPanel_Initialized(object sender, EventArgs e)
        {
        }

        private void TabControl_Initialized(object sender, EventArgs e)
        {
            var s = new Style();
            s.Setters.Add(new Setter(UIElement.VisibilityProperty, Visibility.Collapsed));
            this.MainTabControl.ItemContainerStyle = s;
        }

        private void TextBlock_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
        }

        private void trackvisenabled_Click(object sender, RoutedEventArgs e)
        {
            Settings.Default.trackMe = this.trackvisenabled.IsChecked.Value;
            Settings.Default.Save();
        }

        private void TrackVisitor()
        {
            if (!this.trackvisenabled.IsChecked.Value)
            {
                return;
            }
            string req;
            req = string.Format("https://api.vk.com/method/stats.trackVisitor?access_token{0}", VK_DATA.Token);
            VK_DATA.Request(req);
        }

        /// <summary>
        /// Ивент для кнопки "Изменить статус" UserPanel элемента.
        /// Принудительный вызов возможен - смотреть пример.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <example>UserPanel_ChangeStatus(null, null)</example>
        private void UserPanel_ChangeStatus(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var tq = new UI.TextQuestion("Изменить статус на: ", "Изменение статуса");
            string tqr = tq.ShowDialog();
            if (tqr != null)
            {
                string req = string.Format("https://api.vk.com/method/status.set.xml?text={0}&access_token={1}", tqr, VK_DATA.Token);
                string ans = VK_DATA.Request(req);

                this.UserPanel.UserStatus.Content = tqr;
            }
        }

        private async void UserPanel_Exit(object sender, RoutedEventArgs e)
        {
            this.UserPopup.IsOpen = false;
            this.userNameTextBlock1.Text = "Выход...";
            OFFICIAL_WPF_RU24.Properties.Settings.Default.token = "";
            this.isAuthed = false;
            this.isbytoken = false;
            this.skipvk = false;
            await Task.Factory.StartNew(() => Thread.Sleep(5000));
            this.userNameTextBlock1.Text = "Anonym";
        }

        private void UserPanel_OpenUserPage(object sender, RoutedEventArgs e)
        {
            this.MainTabControl.SelectedIndex = 2;
            this.Browser.Source = new Uri(string.Format("http://vk.com/id{0}", VK_DATA.GetData()[0].Value));
        }
        private void vkGroup_Initialized(object sender, EventArgs e)
        {
        }

        private void WrapPanel_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ChangedButton == System.Windows.Input.MouseButton.Left && e.ClickCount == 2)
            {
                this.Servers_MouseDoubleClick(null, null);
            }
        }

        private void Игра_IsOpenChanged(object sender, EventArgs e)
        {
            //if (!SelectionEnded)
            //    if (!Игра.IsOpen)
            //        App.Current.Shutdown();
        }
        #endregion Events

        #region Exceptions

        public class InvalidServerException : Exception
        {
            public InvalidServerException()
            {
            }

            public InvalidServerException(string message)
                : base(message)
            {
            }

            public InvalidServerException(string message, Exception inner)
                : base(message, inner)
            {
            }

            protected InvalidServerException(
                System.Runtime.Serialization.SerializationInfo info,
                System.Runtime.Serialization.StreamingContext context)
                : base(info, context)
            {
            }
        }

        #endregion Exceptions
    }
}