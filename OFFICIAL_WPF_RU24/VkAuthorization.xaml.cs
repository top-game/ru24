﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Awesomium.Windows.Controls;
using Awesomium;
using Awesomium.Windows.Forms;

namespace OFFICIAL_WPF_RU24
{
    /// <summary>
    /// Interaction logic for VkAuthorization.xaml
    /// </summary>
    public partial class VkAuthorization : Window
    {
        public VkAuthorization()
        {
            InitializeComponent();
        }

        bool isCanceled = false;
        public async Task<VKAPI.Auth.VK> Get()
        {
            VKAPI.Auth.VK VK = new VKAPI.Auth.VK(brows, 4454141, new string[]{
                    VKAPI.TYPES.Rules.Rules_Privilege.account,
                    VKAPI.TYPES.Rules.Rules_Privilege.groups,
                    VKAPI.TYPES.Rules.Rules_Privilege.audio,
                    VKAPI.TYPES.Rules.Rules_Privilege.users,
                    VKAPI.TYPES.Rules.Rules_Privilege.messages,
                    VKAPI.TYPES.Rules.Rules_Privilege.execute
                });
            bool authed = false;
            VKAPI.METHODS.OnAuth += delegate{
                authed = true;
            };
            await Task.Factory.StartNew(delegate{ 
                while(authed != true && isCanceled != true){
                    
                }
            });
            if (this.isCanceled == true) return null;
			else return VK;
        }
            
        

        private void vkIcon_Initialized(object sender, EventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            isCanceled = true;
        }
    }
}
