﻿using System;
using System.Linq;
using System.Windows;

namespace OFFICIAL_WPF_RU24.UI
{
    /// <summary>
    /// Логика взаимодействия для TextQuestion.xaml
    /// </summary>
    public partial class TextQuestion : Window
    {
        public TextQuestion(string text, string caption)
        {
            InitializeComponent();
            this.Title = caption;
            lab1.Content = text;
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            textb1.SpellCheck.IsEnabled = scbox.IsChecked.Value;
        }

        public string ShowDialog()
        {
            base.ShowDialog();
            
            return isCanceled ? null : textb1.Text;
        }
        bool isCanceled = false;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            isCanceled = true;
        }

        private void savebut_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
