﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace OFFICIAL_WPF_RU24.Utilites
{
    public class Dump
    {
        public static void CreateMiniDump()
        {
            using (System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess())
            {
                string FileName = string.Format(@"tgDump_{0}.dmp", DateTime.Now.Ticks);
                
                MINIDUMP_EXCEPTION_INFORMATION Mdinfo = new MINIDUMP_EXCEPTION_INFORMATION();

                Mdinfo.ThreadId = GetCurrentThreadId();
                Mdinfo.ExceptionPointers = Marshal.GetExceptionPointers();
                Mdinfo.ClientPointers = 1;

                using (FileStream fs = new FileStream(FileName, FileMode.Create))
                {
                    {
                        MiniDumpWriteDump(process.Handle, (uint)process.Id, fs.SafeFileHandle.DangerousGetHandle(), MINIDUMP_TYPE.MiniDumpNormal,
               ref Mdinfo,
               IntPtr.Zero,
               IntPtr.Zero);
                    }
                }
            }
        }

        public static void GenerateDump(Exception ex)
        {
            Directory.CreateDirectory(Environment.CurrentDirectory + "\\Dumps\\");

            StreamWriter sw = File.CreateText(Environment.CurrentDirectory + "\\Dumps\\dump_" + DateTime.Now.Date.ToString("DDMMYY") + DateTime.Now.TimeOfDay.TotalMilliseconds + ".tgdmp");
            sw.WriteLine("===========");
            sw.WriteLine("Начало: дамп");
            sw.WriteLine(string.Format("Текущая дата: {0}.{1}.{2} {3}:{4}:{5}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second));
            sw.WriteLine("StackTrace:");
            sw.WriteLine(Environment.StackTrace);
            sw.WriteLine("StackTrace исключения:");
            sw.WriteLine(ex.StackTrace);
            sw.WriteLine("Выполнилось последним:");
            sw.WriteLine(ex.TargetSite);
            sw.WriteLine("Сообщение исключения:");
            sw.WriteLine(ex.Message);
            if (ex.InnerException != null)
            {
                sw.WriteLine("Сведения внутренего исключения:");
                sw.WriteLine(ex.InnerException.Message);
                sw.WriteLine(ex.InnerException.StackTrace);
            }
            sw.WriteLine("Сведения о системе:");
            sw.WriteLine(Environment.OSVersion.VersionString);
            sw.WriteLine("64bitsys: " + Environment.Is64BitOperatingSystem);
            sw.WriteLine("Пользователь (сист.) " + Environment.UserName);
            for (int i = 0; i < Environment.GetCommandLineArgs().Length; i++)
            {
                sw.WriteLine("Аргумент[" + i + "]:" + Environment.GetCommandLineArgs()[i]);
            }

            sw.WriteLine("Конец: дамп");
            sw.WriteLine("===========");
            sw.Close();
            sw.Dispose();
            //Environment.Exit(1);
        }

        [DllImport("kernel32.dll")]
        private static extern uint GetCurrentThreadId();

        [DllImport("Dbghelp.dll")]
        private static extern bool MiniDumpWriteDump(IntPtr hProcess, uint ProcessId, IntPtr hFile, int DumpType, ref MINIDUMP_EXCEPTION_INFORMATION ExceptionParam, IntPtr UserStreamParam, IntPtr CallbackParam);

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct MINIDUMP_EXCEPTION_INFORMATION
        {
            public uint ThreadId;
            public IntPtr ExceptionPointers;
            public int ClientPointers;
        }

        private static class MINIDUMP_TYPE
        {
            public const int MiniDumpNormal = 0x00000000;
        }
    }
}