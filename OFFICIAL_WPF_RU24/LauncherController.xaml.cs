﻿using System.Windows;

namespace OFFICIAL_WPF_RU24
{
    /// <summary>
    /// Interaction logic for LauncherController.xaml
    /// </summary>
    public partial class LauncherController : Window
    {
        public LauncherController()
        {
            this.InitializeComponent();
        }

        private void save_Click(object sender, RoutedEventArgs e)
        {
            var n_mus = new RU24Lib.Types.Extended.MemoryUsageSettings
            {
                LoadMessages = !this.disMES.IsChecked.Value,
                SkipEventsLoading = this.remEVENTS.IsChecked.Value,
                LoadMusic = !this.disMUS.IsChecked.Value
            };
            Properties.Settings.Default.MeUsSet = n_mus;

            var n_cs = new RU24Lib.Types.Extended.ComponentsSettings
            {
                EnableMusic = !this.disMUS.IsChecked.Value,
                EnableVK = !this.emVK.IsChecked.Value
            };

            Properties.Settings.Default.CompSet = n_cs;

            Properties.Settings.Default.Save();

            System.Diagnostics.Process.Start(Application.ResourceAssembly.Location);

            //RETURN RELOAD-CODE
            Application.Current.Shutdown(5);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}